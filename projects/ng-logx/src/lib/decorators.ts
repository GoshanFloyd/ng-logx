import {LogLevel, LogType, NgLogxOptions, NgLogxParameters} from "./types";
import {tap} from "rxjs/operators";
import {isHasOptions} from "./type-guards";
import {prepareOptionsToLog} from "./utils";

const logValue = (value: any, level: LogLevel, options: NgLogxOptions | null) => {
  if (level === LogLevel.INFO) {
    const preparedOptions = options ? prepareOptionsToLog(options) : null;
    const loggedValue = preparedOptions && level === LogLevel.INFO
      ? `%c${value}`
      : value;
    console.log(loggedValue, preparedOptions);
  }

  if (level === LogLevel.WARNING) {
    console.warn(value);
  }
  if (level === LogLevel.ERROR) {
    console.error(value);
  }
}

export const NgLogx = (params: NgLogxParameters) => {
  return function (
    target: Object,
    key: string | symbol,
    descriptor: PropertyDescriptor
  ) {
    const {
      level,
      type,
      enable
    } = params;

    const logLevel = level || LogLevel.INFO;
    const isEnabled = typeof enable !== undefined ? enable : true;
    const options = isHasOptions(params) ? params.options : null;

    const childFunction = descriptor.value;

    if (isEnabled) {
      if (type === LogType.SYNC) {
        descriptor.value = function(...args: any[]) {
          const value = childFunction.apply(this, args);

          logValue(value, logLevel, options);

          return value;
        }
      }
      if (type === LogType.ASYNC) {
        descriptor.value = function(...args: any[]) {
          return childFunction.apply(this, args).pipe(
            tap((value) => {
              logValue(value, logLevel, options);
            })
          );
        }
      }
    }

    return descriptor;
  };
};
